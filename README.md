# Graylog  x  pfSense 

#### credits

First I'd like to say thanks for everyone that contributed to this and I forked from. Unfortunately won't be able to list them all as this / projects like this have been forked around for years now, but thanks!

#### installation / deployment

* IMPORTANT: This repo is to be cloned! It will not work if you just take the compose file alone!
* edit the `GRAYLOG_HTTP_EXTERNAL_URI` in `docker-compose.yml:54` to your Docker host machine's IP (or FQDN). Make sure you have a trailing slash `/`
* Up the stack with `docker-compose up -d --force-recreate`
* Open Graylog at `your.ip.here:9009` and log in with `admin:admin`
* Navigate to `System`-> `Indices` create an index with `Create index set`

  ```
  Title: pfsense-log
  Index prefix: pfsense
  ```

  Leave other parameters as default or adjust to your liking. Choose a rotation strategy and make sure you'll have enough space in the future for the elastic database. Save!
* Navigate to `System` -> `Content Packs` and `Upload` the included `graylog_x_pfsense_cp.json`. Once uploaded, find it in the list and `Install`, having all selected.
* Navigate to `Streams` -> `More action` (at the `PfSense` stream) -> `Edit Stream` and change the `Index Set` to the one you created earlier (`pfsense`)
* If everything went well you can enable external syslog in pfSense to the Docker host IP and a port you chose and mapped in the docker-compose (`4514` default, if you change it make sure you follow changes in `System` -> `Inputs` -> `pfsense` too!)
* In `Streams` -> `pfsense` you should see your logs and mapped fields
